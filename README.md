# RPGHeroes
## Description
Have been created various hero classes having attributes which increase at different rates as the character gains levels. Equipment, such as armor and weapons, that characters can equip. The equipped items will alter the power of the hero, causing it to deal more damage and be able to survive longer. Certain heroes can equip certain item types. Includes testing. 


## Use
VS2022 .NET 6
.NET Core Console Application
xUnit testing

## Documentation
4 classes of heroes.
Heroes have the following methods: Constructor passing a name, LevelUp, Equip with 2 warriants, Damage calculated, Total Attributes and Display on Heroes details

## Contributing
Meg Stefouli

## Licence
MIT
