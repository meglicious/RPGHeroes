using RPG;
using RPGHeroes;
using System.Numerics;


namespace RPGHeroesTest
{
    public class RPGHeroesTest
    {
        [Fact]
        public void ChooseHero_SelectyourHero_RerurnDetails()
        {
            //Arrange
            string name = "Mage";
            int Level = 1;
            Mage hero = new Mage(name);
            //Act
            //Assert
            Assert.Equal(name, hero.Name);
            Assert.Equal(Level, hero.Level);
            Assert.Equal(0, hero.Dexterity);

        }

        [Fact]
        public void HeroLevelUP_ShouldReturnHigherLevelAttributes_Rogue()
        {
            //Warrior
            //Arrange
            string name = "Rogue";
            int Level = 2;
            string experience = "Rogue";
            Rogue myHero = new Rogue(name);
            int[] playerStats = { 3, 10, 2 };
            //Act
            myHero.LevelUp(experience);
            int actualLevel = myHero.Level;
            int[] actualStats = { myHero.Strength, myHero.Dexterity, myHero.Intelligence };
            //Assert
            Assert.Equal(Level, actualLevel);
            Assert.Equal(playerStats, actualStats);

        }
        [Fact]
        public void Weapon_ChooseWeaponType_ShouldHaveNameTypeSlotLevel()
        {
            //Arrange
            string nameEquipment = "Choose weapon";
            int requiredLevel = 1;
            Slot slot = Slot.Weapon;
            WeaponType type = WeaponType.Bow;
            int weaponDamage = 1;
            //Act
            Weapon weapon = new Weapon(nameEquipment, requiredLevel, slot, type, weaponDamage);

            //Assert
            Assert.Equal(nameEquipment, weapon.NameEquipment);
            Assert.Equal(requiredLevel, weapon.RequiredLevel);
            Assert.Equal(slot, weapon.Slot);
            Assert.Equal(type, weapon.Type);
            Assert.Equal(weaponDamage, weapon.WeaponDamage);
        }

        public void Armor_ChooseArmorType_ShouldHaveNameTypeSlotLevel()
        {
            //Arrange
            string nameEquipment = "Choose armor";
            int requiredLevel = 1;
            Slot slot = Slot.Head;
            ArmorType type = ArmorType.Cloth;
            int strength = 1;
            int dexterity = 1;
            int intelligence = 1;
            //Act
            Armor armor = new Armor(nameEquipment, requiredLevel, slot, type, strength, dexterity, intelligence);

            //Assert
            Assert.Equal(nameEquipment, armor.NameEquipment);
            Assert.Equal(requiredLevel, armor.RequiredLevel);
            Assert.Equal(slot, armor.Slot);
            Assert.Equal(type, armor.ArmorType);
            Assert.Equal(strength, armor.Strength);
            Assert.Equal(dexterity, armor.Dexterity);
            Assert.Equal(intelligence, armor.Intelligence);
        }



        [Fact]
        public void WeaponEquipLevelCheck()
        {
            //Arrange
            string name = "Ranger";
            string nameEquipment = "Test weapon";
            int requiredLevel = 1;
            Slot slot = Slot.Weapon;
            WeaponType type = WeaponType.Bow;
            int weaponDamage = 1;
            Ranger myHero = new Ranger(name);
            Weapon weapon = new Weapon(nameEquipment, requiredLevel, slot, type, weaponDamage);
            Inventory inventory = new Inventory();
            //Act
            inventory.EquipWeapon(weapon, myHero);
            //Assert
            Assert.Equal(myHero.Level, weapon.RequiredLevel);
            Assert.Equal(type, weapon.Type);
        }
        [Fact]
        public void ArmorEquipLevelCheck()
        {
            //Arrange
            string name = "Mage";
            string nameEquipment = "Test armor";
            int requiredLevel = 1;
            Slot slot = Slot.Head;
            ArmorType type = ArmorType.Cloth;
            int strength = 1;
            int dexterity = 1;
            int intelligence = 1;
            Mage myHero = new Mage(name);
            Armor armor = new Armor(nameEquipment, requiredLevel, slot, type, strength, dexterity, intelligence);
            Inventory inventory = new Inventory();
            //Act
            inventory.EquipArmor(armor, myHero);
            //Assert
            Assert.Equal(myHero.Level, armor.RequiredLevel);
            Assert.Equal(ArmorType.Cloth, armor.ArmorType);
        }
        [Fact]
        public void TotalAttributes_NoEquipment()
        {
            //Arrange
            string name = "Mage";
            Mage myHero = new Mage(name);
            Armor beginnerHead = new Armor("Beginner Hat", 0, Slot.Head, ArmorType.Starter, 0, 0, 0);
            Armor beginnerBody = new Armor("Beginner Shirt", 0, Slot.Body, ArmorType.Starter, 0, 0, 0);
            Armor beginnerLegs = new Armor("Beginner Pants", 0, Slot.Legs, ArmorType.Starter, 0, 0, 0);
            Inventory inventory = new Inventory();
            inventory.EquipArmor(beginnerHead, myHero);
            inventory.EquipArmor(beginnerBody, myHero);
            inventory.EquipArmor(beginnerLegs, myHero);
            int expectedStr = 1;
            int expectedDex = 1;
            int expectedInt = 8;
            //Act
            myHero.TotalStrength();
            //Assert
            Assert.Equal(expectedStr, myHero.TotalStrength());
            Assert.Equal(expectedDex, myHero.TotalDexterity());
            Assert.Equal(expectedInt, myHero.TotalIntelligence());
        }

        [Fact]
        public void TotalAttributes_OneEquipment()
        {
            //Arrange
            string name = "Mage";
            Mage myHero = new Mage(name);
            Armor beginnerBody = new Armor("Beginner Shirt", 0, Slot.Body, ArmorType.Starter, 0, 0, 0);
            Armor beginnerLegs = new Armor("Beginner Pants", 0, Slot.Legs, ArmorType.Starter, 0, 0, 0);
            Armor beginnerHead = new Armor("Cloth Hat", 3, Slot.Head, ArmorType.Cloth, 0, 1, 3);
            Inventory inventory = new Inventory();
            inventory.EquipArmor(beginnerHead, myHero);
            inventory.EquipArmor(beginnerBody, myHero);
            inventory.EquipArmor(beginnerLegs, myHero);
            int expectedStr = 1;
            int expectedDex = 2;
            int expectedInt = 11;
            //Act
            myHero.TotalStrength();
            //Assert
            Assert.Equal(expectedStr, myHero.TotalStrength());
            Assert.Equal(expectedDex, myHero.TotalDexterity());
            Assert.Equal(expectedInt, myHero.TotalIntelligence());
        }

        public void TotalAttributes_TwoEquipment()
        {
            //Arrange
            string name = "Mage";
            Mage myHero = new Mage(name);
            Armor beginnerLegs = new Armor("Beginner Pants", 0, Slot.Legs, ArmorType.Starter, 0, 0, 0);
            Armor beginnerHead = new Armor("Cloth Hat", 0, Slot.Head, ArmorType.Cloth, 0, 1, 3);
            Armor beginnerBody = new Armor("Cloth Shirt", 0, Slot.Body, ArmorType.Cloth, 0, 1, 5);
            Inventory inventory = new Inventory();
            inventory.EquipArmor(beginnerHead, myHero);
            inventory.EquipArmor(beginnerBody, myHero);
            inventory.EquipArmor(beginnerLegs, myHero);
            int expectedStr = 1;
            int expectedDex = 3;
            int expectedInt = 16;
            //Act
            myHero.TotalStrength();
            //Assert
            Assert.Equal(expectedStr, myHero.TotalStrength());
            Assert.Equal(expectedDex, myHero.TotalDexterity());
            Assert.Equal(expectedInt, myHero.TotalIntelligence());
        }


        [Fact]
        public void TotalAttributes_StarterWeapon()
        {
            //Arrange
            string name = "Warrior";
            Mage myHero = new Mage(name);
            Weapon beginnerMageWeapon = new Weapon("Beginner Wand", 0, Slot.Weapon, WeaponType.Wand, 1);
            Inventory inventory = new Inventory();
            inventory.EquipWeapon(beginnerMageWeapon, myHero);
            double totalWeaponDamage = 1.08;
            //Act
            myHero.TotalDamage();
            //Assert
            Assert.Equal(totalWeaponDamage, myHero.TotalDamage());
        }
    }
}