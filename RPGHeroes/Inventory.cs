﻿using RPG;
using System.Collections.Generic;
using System.Numerics;
using System.Security.Cryptography.X509Certificates;

namespace RPGHeroes
{
    public class Inventory
    {
        List<Weapon> weapons;
        List<Armor> armors;

        public Inventory()
        {
            // weapon List
            weapons = new List<Weapon>
            {
                new Weapon("Wooden Sword", 1, Slot.Weapon, WeaponType.Sword, 2),
                new Weapon("Wooden Bow", 1, Slot.Weapon, WeaponType.Bow, 2),
                new Weapon("Wooden Staff", 1, Slot.Weapon, WeaponType.Staff, 2),
                new Weapon("Wooden Dagger", 1, Slot.Weapon, WeaponType.Dagger, 2),
                new Weapon("Wooden Axe", 1, Slot.Weapon, WeaponType.Axe, 2),
                new Weapon("Wooden Hammer", 1, Slot.Weapon, WeaponType.Hammer, 2),
                new Weapon("Wooden Wand", 1, Slot.Weapon, WeaponType.Wand, 2),

                new Weapon("Iron Sword", 3, Slot.Weapon, WeaponType.Sword, 4),
                new Weapon("Iron Bow", 3, Slot.Weapon, WeaponType.Bow, 4),
                new Weapon("Iron Staff", 3, Slot.Weapon, WeaponType.Staff, 4),
                new Weapon("Iron Dagger", 3, Slot.Weapon, WeaponType.Dagger, 4),
                new Weapon("Iron Axe", 3, Slot.Weapon, WeaponType.Axe, 4),
                new Weapon("Iron Hammer", 3, Slot.Weapon, WeaponType.Hammer, 4),
                new Weapon("Iron Wand", 3, Slot.Weapon, WeaponType.Wand, 4),

            };
            // armor list
            armors = new List<Armor>
            {
                new Armor("Cloth Hat", 1, Slot.Head, ArmorType.Cloth, 0, 1, 3),
                new Armor("Cloth Shirt", 2, Slot.Body, ArmorType.Cloth, 0, 1, 5),
                new Armor("Cloth Pants", 3, Slot.Legs, ArmorType.Cloth, 0, 1, 8),

                new Armor("Leather Hat", 2, Slot.Head, ArmorType.Leather, 1, 3, 0),
                new Armor("Leather Shirt", 3, Slot.Body, ArmorType.Leather, 2, 5, 0),
                new Armor("Leather Pants", 4, Slot.Legs, ArmorType.Leather, 3, 8, 0),

                new Armor("Mail Hat", 3, Slot.Head, ArmorType.Mail, 3, 0, 0),
                new Armor("Mail Shirt", 4, Slot.Body, ArmorType.Mail, 5, 0, 0),
                new Armor("Mail Pants", 5, Slot.Legs, ArmorType.Mail, 8, 0, 0),

                new Armor("Plate Hat", 4, Slot.Head, ArmorType.Plate, 5, 0, 0),
                new Armor("Plate Shirt", 5, Slot.Body, ArmorType.Plate, 8, 0, 0),
                new Armor("Plate Pants", 6, Slot.Legs, ArmorType.Plate, 11, 0, 0),

            };
        }
        internal List<Weapon> Weapons { get => weapons; set => weapons = value; }
        internal List<Armor> Armors { get => armors; set => armors = value; }

        public void DisplayWeaponItems()
        {
            Console.WriteLine("Inventory:");
            for (int i = 0; i < weapons.Count; i++)
            {
                Console.WriteLine((i + 1) + ". " + weapons[i].NameEquipment);
            }
        }
        public void DisplayArmorItems()
        {
            Console.WriteLine("Inventory:");
            for (int j = 0; j < Armors.Count; j++)
            {
                Console.WriteLine((j + 1) + ". " + Armors[j].NameEquipment);
            }
        }

        //equip weapon
        public virtual void EquipWeapon(Weapon weapon, MyHero myHero)
        {
            if (myHero.Experience == "Mage" && (weapon.Type == WeaponType.Staff || weapon.Type == WeaponType.Wand))
            {
                UpgradeWeapon(weapon, myHero, Slot.Weapon);
            }
            else if (myHero.Experience == "Ranger" && weapon.Type == WeaponType.Bow)
            {
                UpgradeWeapon(weapon, myHero, Slot.Weapon);
            }
            else if (myHero.Experience == "Rogue" && (weapon.Type == WeaponType.Dagger || weapon.Type == WeaponType.Sword))
            {
                UpgradeWeapon(weapon, myHero, Slot.Weapon);
            }
            else if (myHero.Experience == "Warrior" && (weapon.Type == WeaponType.Axe || weapon.Type == WeaponType.Hammer || weapon.Type == WeaponType.Sword))
            {
                UpgradeWeapon(weapon, myHero, Slot.Weapon);
            }
            else
            {
                Console.WriteLine("Please choose the right weapon for your experience");
            }
        }

        private void UpgradeWeapon(Weapon weapon, MyHero myHero, Slot slot)
        {
            if (myHero.Equipment.ContainsKey(slot))
            {
                myHero.Equipment.Remove(slot);
            }
            if (weapon.RequiredLevel <= myHero.Level)
            {
                myHero.Equipment.Add(weapon.Slot, weapon);
                Console.WriteLine($"{myHero.Name} equipped an {myHero.Equipment[weapon.Slot].NameEquipment}");
            }
            else
            {
                Console.WriteLine($"{myHero.Name} your level is too low to equip {weapon.NameEquipment}");
            }
        }



        //equip armor
        public virtual void EquipArmor(Armor armor, MyHero myHero)
        {
            if (myHero.Experience == "Mage" && (armor.ArmorType == ArmorType.Cloth || armor.ArmorType == ArmorType.Starter))
            {
                UpgradeArmor(armor, myHero, Slot.Head);
            }
            else if ((myHero.Experience == "Ranger" || myHero.Experience == "Rogue") && (armor.ArmorType == ArmorType.Leather || armor.ArmorType == ArmorType.Mail || armor.ArmorType == ArmorType.Starter))
            {
                UpgradeArmor(armor, myHero, Slot.Body);
            }
            else if (myHero.Experience == "Warrior" && (armor.ArmorType == ArmorType.Plate || armor.ArmorType == ArmorType.Mail || armor.ArmorType == ArmorType.Starter))
            {
                UpgradeArmor(armor, myHero, Slot.Legs);
            }
            else
            {
                Console.WriteLine($"Please choose an armor that corresponds with your class");
            }
        }
        private void UpgradeArmor(Armor armor, MyHero myHero, Slot slot)
        {
            if (myHero.Equipment.ContainsKey(slot))
            {
                myHero.Equipment.Remove((Slot)armor.ArmorType);
                if (armor.RequiredLevel <= myHero.Level)
                {
                    myHero.Equipment.Add((Slot)armor.ArmorType, armor);
                    Console.WriteLine($"{myHero.Name} equipped an {myHero.Equipment[(Slot)armor.ArmorType].NameEquipment}");
                }
                else
                {
                    Console.WriteLine($"{myHero.Name} your level is too low to equip {armor.NameEquipment}");
                }
            }
            else
            {
                Console.WriteLine("else");
                myHero.Equipment.Add(armor.Slot, armor);
                Console.WriteLine($"{myHero.Name} equipped an {myHero.Equipment[armor.Slot].NameEquipment}");
            }
        }

        internal void EquipArmor(Armor beginnerHead)
        {
            throw new NotImplementedException();
        }

        internal void EquipWeapon(Weapon beginnerMageWeapon)
        {
            throw new NotImplementedException();
        }
    }
}