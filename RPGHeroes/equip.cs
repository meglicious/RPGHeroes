﻿using System;
using static RPGHeroes.ChooseHero;

namespace RPGHeroes
{
	public class Equip
	{
        private string nameEquipment;
        private int requiredLevel;
        private Slot slot;

        public Equip()
		{

		}

        public Equip(string nameEquipment, int requiredLevel, Slot slot)
        {
            NameEquipment = nameEquipment;
            RequiredLevel = requiredLevel;
            Slot = slot;

        }
        public string NameEquipment { get => nameEquipment; set => nameEquipment = value; }
        public int RequiredLevel { get => requiredLevel; set => requiredLevel = value; }
        public Slot Slot { get => slot; set => slot = value; }
    }
}

