﻿using System;
namespace RPGHeroes
{
    public class Ranger : MyHero
    {

        public Ranger(string name)
        {
            Level = 1;
            Name = "Ranger";
            Strength = 1;
            Dexterity = 7;
            Intelligence = 1;

        }
    }
}

