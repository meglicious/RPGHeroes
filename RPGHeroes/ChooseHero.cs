﻿using System;
using RPG;
using System.Numerics;

namespace RPGHeroes
{
    internal class ChooseHero
    {
        public ChooseHero()
        {
            //Select a hero
            Console.WriteLine("Choose your Hero name: Mage, Ranger, Rogue or Warrior ");
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
            string name = Console.ReadLine();
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.

            // declares an 1D Array of name & allocating memory for Heroes names.
            MyHero[] heroes = new MyHero[4];
#pragma warning disable CS8604 // Possible null reference argument.
            heroes[0] = new Mage(name);
#pragma warning restore CS8604 // Possible null reference argument.
            heroes[1] = new Ranger(name);
            heroes[2] = new Rogue(name);
            heroes[3] = new Warrior(name);

            // class selection
            Console.WriteLine("Choose a experience:");
            for (int i = 0; i < heroes.Length; i++)
            {
                Console.WriteLine((i + 1) + heroes[i].Experience);
            }

            int heroArray = Convert.ToInt16(Console.ReadLine());
            MyHero myHero = heroes[heroArray - 1];

            //starter weapons
            Weapon beginnerMageWeapon = new Weapon("Beginner Wand", 0, Slot.Weapon, WeaponType.Wand, 1);
            Weapon beginnerRangerWeapon = new Weapon("Beginner Slingshot", 0, Slot.Weapon, WeaponType.Bow, 1);
            Weapon beginnerRogueWeapon = new Weapon("Beginner Sword", 0, Slot.Weapon, WeaponType.Sword, 1);
            Weapon beginnerWarriorWeapon = new Weapon("Beginner Sword", 0, Slot.Weapon, WeaponType.Sword, 1);
            //starter armor
            Armor beginnerHead = new Armor("Beginner Hat", 0, Slot.Head, ArmorType.Starter, 0, 0, 0);
            Armor beginnerBody = new Armor("Beginner Top", 0, Slot.Body, ArmorType.Starter, 0, 0, 0);
            Armor beginnerLegs = new Armor("Beginner Boots", 0, Slot.Legs, ArmorType.Starter, 0, 0, 0);

            Inventory inventory = new Inventory();
            //total starter items
            inventory.EquipArmor(beginnerHead, myHero);
            inventory.EquipArmor(beginnerBody, myHero);
            inventory.EquipArmor(beginnerLegs, myHero);

            if (myHero.experience == "Mage")
            {
                inventory.EquipWeapon(beginnerMageWeapon);
            }
            else if (myHero.experience == "Ranger")
            {
                inventory.EquipWeapon(beginnerRangerWeapon);
            }
            else if (myHero.experience == "Rogue")
            {
                inventory.EquipWeapon(beginnerRogueWeapon);
            }
            else if (myHero.experience == "Warrior")
            {
                inventory.EquipWeapon(beginnerWarriorWeapon);
            }

            //Open next menu
            new Options(myHero);


        }

        public class Slot
        {
            public static RPGHeroes.Slot Weapon { get; internal set; }
            public static RPGHeroes.Slot Head { get; internal set; }
            public static RPGHeroes.Slot Body { get; internal set; }
            public static RPGHeroes.Slot Legs { get; internal set; }
        }
    }
}