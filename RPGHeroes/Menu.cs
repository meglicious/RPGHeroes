﻿using RPGHeroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes
{
    internal class Menu
    {


        public Menu(MyHero myHero)
        {
            Options options = new Options();

            //option menu presented
            while (true)
            {
                options.MenuDisplay();
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
                string OptionChoice = Console.ReadLine();
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.
                if (OptionChoice == "1")
                {
                    MyHero.LevelUp(myHero);
                }
                else if (OptionChoice == "2")
                {
                    options.InventoryMenu(myHero);
                }
                else if (OptionChoice == "3")
                {
                    options.HeroDetails(myHero);

                }
                else
                {
                    Console.WriteLine("Choose another option!");
                }

            }
        }
    }
}


        

  