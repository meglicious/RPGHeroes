﻿using System;
using System.Collections.Generic;

namespace RPGHeroes
{
    public class Armor : Equip
    {
        public ArmorType ArmorType { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public Armor(string nameEquipment, int requiredLevel, Slot slot, ArmorType armorType, int strength, int dexterity, int intelligence) : base(nameEquipment, requiredLevel, slot)
        {
            this.ArmorType = armorType;
            this.Strength = strength;
            this.Dexterity = dexterity;
            this.Intelligence = intelligence;
        }
    }
    public enum ArmorType
    {
        Starter,
        Cloth,
        Leather,
        Mail,
        Plate
    }

}

