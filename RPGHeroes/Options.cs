﻿using RPG;
using System.ComponentModel;
using System.Numerics;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Linq;

namespace RPGHeroes
{
    internal class Options
    {
        public Options()
        {

        
        }

        public Options(MyHero myHero)
        {
        }

        //Create a Menu to choose Level, Inventory and Details
        internal void MenuDisplay()
        {
            Console.WriteLine("Menu\n Choose a number from bellow:\n 1. Level Up\n 2. Inventory\n 3. Display Hero details");
        }
        internal void InventoryMenu(MyHero myHero)
        {
            Console.WriteLine("Choose from Inventory:1 for Weapon or 2 for Armor");
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
            string OptionChoice = Console.ReadLine();
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.
            Inventory inventory = new Inventory();
            if (OptionChoice == "1")
            {
                inventory.DisplayWeaponItems();
                Console.WriteLine("Enter the number of the item you want to equip:");
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
                string equipChoice = Console.ReadLine();
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.
                int choices = Convert.ToInt32(equipChoice);
                if (choices < 0 || choices > inventory.Weapons.Count)
                {
                    Console.WriteLine("Choose a valid number.");
                    InventoryMenu(myHero);
                }
                else
                {
                    Weapon weapon = inventory.Weapons[choices - 1];
                    inventory.EquipWeapon(weapon, myHero);
                }
            }
            else if (OptionChoice == "2")
            {
                inventory.DisplayArmorItems();
                Console.WriteLine("Enter the number of the item you want to equip:");
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
                string equipChoice = Console.ReadLine();
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.
                int choices = Convert.ToInt32(equipChoice);
                if (choices < 0 || choices > inventory.Armors.Count)
                {
                    Console.WriteLine("Choose a valid number.");
                    InventoryMenu(myHero);
                }
                else
                {
                    Armor armor = inventory.Armors[choices - 1];
                    inventory.EquipArmor(armor, myHero);
                }

            }

        }

 
        //Display the Hero Details
        internal void HeroDetails(MyHero myHero)
        {
            Console.WriteLine($"You choose: {myHero.Name}\n Your hero has:\n Level: {myHero.Level}\n Total Strenght: {myHero.TotalStrength}\n Total Dexterity: {myHero.TotalDexterity}\n Total Intelligence: {myHero.TotalIntelligence}\n Total Experience: {myHero.Experience}");
            Console.WriteLine("Back to Menu, press any key");
            Console.ReadLine();
        }

        
    }
}