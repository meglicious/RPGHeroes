﻿using System;
namespace RPGHeroes
{
    public class Mage : MyHero
    {

        public Mage(string name)
        {   
            Level = 1;
            Name = "Mage";
            Strength = 1;
            Dexterity = 1;
            Intelligence = 8;

        }
    }
}

