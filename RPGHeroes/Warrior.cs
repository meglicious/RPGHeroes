﻿using System;
namespace RPGHeroes
{
	public class Warrior : MyHero
	{
        
        public Warrior(string name)
		{
            Level = 1;
            Name = "Warrior";
            Strength = 5;
            Dexterity = 2;
            Intelligence = 1;

        }
    }
}

