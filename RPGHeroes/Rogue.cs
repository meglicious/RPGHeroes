﻿using System;
namespace RPGHeroes
{
    public class Rogue : MyHero
    {

        public Rogue(string name)
        {
            Level = 1;
            Name = "Rogue";
            Strength = 2;
            Dexterity = 6;
            Intelligence = 1;

        }
    }
}

