﻿using RPGHeroes;
using System;
using System.Collections.Generic;

namespace RPG
{
    public class Weapon : Equip
    {
        public WeaponType type;
        public int weaponDamage = 1;

        public Weapon(string nameEquipment, int requiredLevel, Slot slot, WeaponType type, int weaponDamage) : base(nameEquipment, requiredLevel, slot)
        {
            this.Type = type;
            this.WeaponDamage = weaponDamage;
        }

   
      
        public WeaponType Type { get; set; }
        public int WeaponDamage { get; set; }

    }

}

public enum WeaponType //encapsulate these in a WeaponType enumerator
{
    Axe,
    Bow,
    Dagger,
    Hammer,
    Staff,
    Sword,
    Wand
}




