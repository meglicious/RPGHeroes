﻿using RPG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace RPGHeroes
{
	public class MyHero
	{
		public int level;
        public string experience;
		public string name;
        public int strength;
        public int dexterity;
        public int intelligence;
        private Dictionary<Slot, Equip> equipment = new Dictionary<Slot, Equip>();
        

        public string Name { get => name; set => name = value; }
        public string Experience { get => experience; set => experience = value; }
        public int Level { get => level; set => level = value; }
        public int Strength { get => strength; set => strength = value; }
        public int Dexterity { get => dexterity; set => dexterity = value; }
        public int Intelligence { get => intelligence; set => intelligence = value; }
        public Dictionary<Slot, Equip> Equipment { get => equipment; set => equipment = value; }


        //increase player level + increase experience 
        public void LevelUp(string experience)
        {
            Level++;
           
            if (experience == "Mage")
            {
                Strength++;
                Dexterity++;
                Intelligence += 5;
            }
            else if (experience == "Ranger")
            {
                Strength++;
                Dexterity += 5;
                Intelligence++;
            }
            else if (experience == "Rogue")
            {
                Strength++;
                Dexterity += 4;
                Intelligence++;
            }
            else if (experience == "Warrior")
            {
                Strength += 3;
                Dexterity += 2;
                Intelligence++;
            }
        }


#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public MyHero()
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        {
       
            Level= 1;
            this.Level= 1;
#pragma warning disable CS8601 // Possible null reference assignment.
            Name = name;
#pragma warning restore CS8601 // Possible null reference assignment.
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;

        }

        public double TotalDamage()
        {
            int weapon = ((Weapon)Equipment[Slot.Weapon]).WeaponDamage;
            double attribute;
            switch (Experience)
            {
                case "Mage":
                    attribute = Intelligence;
                    break;
                case "Warrior":
                    attribute = Strength;
                    break;
                case "Ranger":
                case "Rogue":
                    attribute = Dexterity;
                    break;
                default:
                    throw new ArgumentException("Not enough Experience");
            }
            return weapon * (1 + (attribute / 100));
        }


        public virtual double TotalStrength()
        {
            int head = ((Armor)Equipment[Slot.Head]).Strength;
            int body = ((Armor)Equipment[Slot.Body]).Strength;
            int legs = ((Armor)Equipment[Slot.Legs]).Strength;
            return Strength + head + body + legs;

        }
        public virtual double TotalDexterity()
        {
            int head = ((Armor)Equipment[Slot.Head]).Dexterity;
            int body = ((Armor)Equipment[Slot.Body]).Dexterity;
            int legs = ((Armor)Equipment[Slot.Legs]).Dexterity;
            return Dexterity + head + body + legs;

        }
        public virtual double TotalIntelligence()
        {
            int head = ((Armor)Equipment[Slot.Head]).Intelligence;
            int body = ((Armor)Equipment[Slot.Body]).Intelligence;
            int legs = ((Armor)Equipment[Slot.Legs]).Intelligence;
            return Intelligence + head + body + legs;

        }

        internal static void LevelUp(MyHero myHero)
        {
            Console.WriteLine("Your " + myHero + " is growing!");

            Console.WriteLine("You leveled up!!!");
            MyHero.LevelUp(myHero);
            Console.WriteLine($"Your are now level {myHero.Level}");
            Console.WriteLine($"your hero strength is: {myHero.Strength}");
            Console.WriteLine($"your hero dexterity is: {myHero.Dexterity}");
            Console.WriteLine($"your hero intelligence is: {myHero.Intelligence}");
        }
    }
}

